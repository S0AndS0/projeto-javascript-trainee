alert("Teste seus conhecimentos sobre os Valores da GTi <3")

let Questions;
httpRequest = new XMLHttpRequest();

httpRequest.open('GET', 'https://quiz-trainee.herokuapp.com/questions', true);
httpRequest.send();

httpRequest.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
        Questions = (JSON.parse(this.responseText));
    }

}

let startButton;
let nextButton;
let tituloElement;
let listaRespostasElement;

window.addEventListener('load', (_event) => {
  startButton = document.getElementById('start-btn');
  nextButton = document.getElementById('next-btn');
  tituloElement = document.getElementById('titulo');
  listaRespostasElement = document.getElementById('listaRespostas');

  startButton.addEventListener('click', startGame)
  nextButton.addEventListener('click', setNextQuestion)
});


let currentQuestion = -1;
let score = 0;


function startGame() {
    if (currentQuestion == -1) {
        startButton.classList.add('hide')
        //tituloElement.classList.remove('hide')
        listaRespostasElement.classList.remove('hide')
        nextButton.classList.remove('hide')
        currentQuestion++;
    }

    document.getElementById('titulo').innerHTML = Questions[currentQuestion].title;

    let spans = document.getElementsByTagName('span');
    let spansRespostas = document.getElementsByName('resposta');
    document.getElementById('listaRespostas').style.display = 'block';

    for (let i = 0; i < Questions[currentQuestion].options.length; i++) {
        const options = Questions[currentQuestion].options[i];

        spans[i].innerHTML = options.answer;
        spansRespostas[i].value = options.value;

        spansRespostas[i].innerHTML = options.answer
        spansRespostas[i].parentElement.children[0].value = options.value
    }
}

function setNextQuestion() {
    // @todo?
    //  - check if radio elements exsist
    //  - if radio element is selected
    //  - if selected radio is correct answer
    startGame(Questions[currentQuestion])
}
